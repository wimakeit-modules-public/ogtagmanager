<?php

namespace Wimakeit\OgTagManager\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\Json\EncoderInterface as JsonEncoderInterface;

/**
 * Project          :      OG Tag Manager
 * File Name        :      View.php
 * Created By       :      Laurent Snackaert | laurent@wimakeit.com
 * Date             :      26/08/2022 / 09:38
 * Company          :      WiMakeIt / https://www.wimakeit.com
 * IDE              :      PhpStorm
 */
class View extends \Magento\Catalog\Block\Product\View
{

    public function __construct(Context $context, EncoderInterface $urlEncoder, JsonEncoderInterface $jsonEncoder, StringUtils $string, Product $productHelper, ConfigInterface $productTypeConfig, FormatInterface $localeFormat, Session $customerSession, ProductRepositoryInterface $productRepository, PriceCurrencyInterface $priceCurrency, array $data = [])
    {
        parent::__construct($context, $urlEncoder, $jsonEncoder, $string, $productHelper, $productTypeConfig, $localeFormat, $customerSession, $productRepository, $priceCurrency, $data);

        $this->priceCurrency = $priceCurrency;
    }

    /**
     * GET currency code
     * @return mixed
     */
    public function getCurrencyCode(){

        return $this->priceCurrency->getCurrency()->getCurrencyCode();

    }


}
