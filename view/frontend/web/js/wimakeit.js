require([
    'jquery',
    'domReady!'
], function ($) {
    let timeouts = {};

    // WaitEl function to wait for the element to load
    function waitEl(selector, callback) {
        if ($(selector).length) {
            callback();
            // clear the timeout to avoid perf leaks
            clearTimeout(timeouts[selector]);
            //console.log("clear timeout", selector);
        } else {
            timeouts[selector] = setTimeout(function () {
                waitEl(selector, callback);
            }, 100);
        }
    }

    // Wait for the fotorama image to load and add itemprop="image"
    waitEl('.fotorama__img', function () {
        var $fotoramaImg = $(".fotorama__img");
        $fotoramaImg.attr("itemprop", "image");
    });
});
