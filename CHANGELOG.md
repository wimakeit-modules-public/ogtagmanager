### Changelog

All notable changes to this project will be documented in this file. Dates are displayed in UTC.

#### [1.0.6](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.5...1.0.6)

> 24 November 2022

- Change readme version of tag [`01e84f6`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/01e84f6b247b588f734b7f2693579ca4ae26bd48)
- Update composer version [`2afe38a`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/2afe38ac41268b9ed6c13531cbff664836fe3b76)
- Remove currency code in price tag [`3327bd5`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/3327bd566d5a7421f0006007f27c2ee7972e93ce)

#### [1.0.5](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.4...1.0.5)

> 26 August 2022

- Fix minor issues [`e4b129b`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/e4b129ba6bfb7e319bb6eec6fccc94cb0daedfd1)

#### [1.0.4](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.3...1.0.4)

> 26 August 2022

- Fix namespace and di [`348907b`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/348907b41924602f5ff74a17eb721af29a6b0309)
- Update version composer and readme [`fdef49d`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/fdef49d438cb5b910adf51295fdad9f77ff70699)

#### [1.0.3](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.2...1.0.3)

> 26 August 2022

- Fix issue context [`e4eb786`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/e4eb7866f1e1eeb0461533b88d025e18f52be87f)

#### [1.0.2](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.1...1.0.2)

> 26 August 2022

- Update README.md [`#1`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/merge_requests/1)
- Update for dynamic currency code and formatted price [`f3dcd0a`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/f3dcd0ab6856e8079bf397fe248b583f76e93cd8)

#### [1.0.1](https://gitlab.com/wimakeit-modules-public/ogtagmanager/compare/1.0.0...1.0.1)

> 26 August 2022

- Update price tag and currency tag [`a065d38`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/a065d382e2e7ec03d4a2efb95c9de6f4ece5ba89)
- Fix minor issues [`2fcc8f0`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/2fcc8f05ef90b31761e9d515bd51cbc755954821)

#### 1.0.0

> 19 August 2022

- Create module og [`11f2f13`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/11f2f13f6662c76fa7ea1fc7aec4f91fcf14ad33)
- Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist [`01a89e8`](https://gitlab.com/wimakeit-modules-public/ogtagmanager/commit/01a89e8fb79e778d409907c4f2bdf4055e61ae54)
